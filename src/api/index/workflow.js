
export function startprocess() {
    return axios({
        url: '/activiti/task/startprocess',
        method: 'post'
    })
}

export function completeTask() {
    return axios({
        url: '/activiti/task/complete',
        method: 'post'
    })
}

/**
 * 获取请假申请表单数据
 * @returns {*}
 */
export function getLeaveRequestFormData(params){
    return axios({
        url: '/pagelist/commonpage/de4a',
        method: 'post',
        params:params,
    })
}
